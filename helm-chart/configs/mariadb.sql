DROP DATABASE IF EXISTS reconstruct;

CREATE DATABASE reconstruct;

grant all privileges on reconstruct.* TO '3dclient'@'%' identified by '3dclient';

flush privileges;

USE reconstruct;

DROP TABLE IF EXISTS reconstruction;

CREATE TABLE reconstruction(  
requestid VARCHAR(100) NOT NULL,  
status VARCHAR(100) NOT NULL,
totalchunks int ,
PRIMARY KEY ( requestid ));  

CREATE TABLE reconstruction_progress(  
requestid VARCHAR(100) NOT NULL,  
sequence int NOT NULL,
progress VARCHAR(100),
FOREIGN KEY ( requestid ) REFERENCES reconstruction (requestid));  

# 3dreconstruct

![Architecture](./docs/3dreconstruct-skupper.JPG "Architecture")

Deploy NFS provisioner on both device and cloud cluster using below command

```
helm repo add nfs-ganesha-server-and-external-provisioner https://kubernetes-sigs.github.io/nfs-ganesha-server-and-external-provisioner/
helm install my-release nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner
```

Create secret for docker-registry 
```
kubectl create secret docker-registry regcred --docker-server=ci.tno.nl:4567 --docker-username=gitlab-ci-token --docker-password=<token> -n reconstruct
```

Deploy application to cloud / TNO cluster. Run these command from ./helm-charts directory.

```
helm dependency update
helm --kube-context=<context name>  -f .\values-server.yaml -n reconstruct  install reconstruct . --create-namespace
```

Deploy application to device cluster

```
helm --kube-context=<context name>  -f .\values-client.yaml -n reconstruct  install reconstruct . --create-namespace
```

### Multicluster Failover configuration using Skupper

Install [Skupper CLI](https://skupper.io/start/index.html#step-1-install-the-skupper-commandline-tool-in-your-environment)
Install Skupper on Cloud cluster.

```
skupper --context=<context name> init -n reconstruct
skupper --context=<context name> token create .\awstoken -n reconstruct
```

Install Skupper on device cluster

```
skupper init --context=<context name> --ingress none -n reconstruct
skupper --context=<context name> link create .\awstoken -n reconstruct
```

Expose cloud cluster services to device cluster. Run below command with cloud cluster context.

```
skupper --context=<context name> expose service openmvg -n reconstruct --address openmvgremote
skupper --context=<context name> expose service otel-collector -n reconstruct --address otel-collector-remote
```

Install nginx loadbalancer on device cluster
```
kubectl apply -f .\config\nginxlb.yml -n reconstruct
```

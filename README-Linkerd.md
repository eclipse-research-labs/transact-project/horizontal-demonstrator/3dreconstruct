# 3dreconstruct

![Architecture](./docs/3d reconstruction Architecture.PNG "Architecture")

Deploy application to cloud / TNO instance. Run these command from ./helm-charts directory.

```
helm --kube-context=<context name>  -f .\values-server.yaml -n reconstruct  install reconstruct . --create-namespace
```

Deploy application to client instance

```
helm --kube-context=<context name>  -f .\values-client.yaml -n reconstruct  install reconstruct . --create-namespace
```


### Multicluster Failover configuration using Linkerd

Create create SSL certificates for trust anchor using step utility. Refer link (https://smallstep.com/docs/step-cli/installation/) for step installation. 

```
step certificate create root.linkerd.cluster.local root.crt root.key --profile root-ca --no-password --insecure

step certificate create identity.linkerd.cluster.local issuer.crt issuer.key --profile intermediate-ca --not-after 8760h --no-password --insecure --ca root.crt --ca-key root.key
```

Install Linkerd CLI (https://linkerd.io/2.13/getting-started/#step-1-install-the-cli)

Install Linkered on both cluster using below commands. Change the context names accordingly. Below commands use "minikube" context for client cluster and "tnok8s" context for cloud/server cluster Run below command from the path where SSL certificates are created.

```
# Install Linkerd CRD's
linkerd --context=tnok8s install --crds | kubectl --context=tnok8s apply -f -
linkerd --context=minikube install --crds | kubectl --context=minikube apply -f -

# Install trust anchors
linkerd --context=tnok8s install --identity-trust-anchors-file root.crt --identity-issuer-certificate-file issuer.crt --identity-issuer-key-file issuer.key | kubectl --context=tnok8s apply -f -

linkerd --context=minikube install --set proxyInit.runAsRoot=true --identity-trust-anchors-file root.crt --identity-issuer-certificate-file issuer.crt --identity-issuer-key-file issuer.key | kubectl --context=minikube apply -f -

# Install Linkerd multicluster externsion
linkerd --context=tnok8s multicluster install | kubectl --context=tnok8s apply -f -

linkerd --context=minikube multicluster install | kubectl --context=minikube apply -f -

# Link the 2 clusters
linkerd --context=tnok8s multicluster link --cluster-name tnok8s | kubectl --context=minikube apply -f -

# Validate the instllation
linkerd --context=minikube  multicluster check

```

Install SMI and failover extensions for Llinkerd

```
helm repo add linkerd-smi https://linkerd.github.io/linkerd-smi
helm repo up
helm install --kube-context=minikube linkerd-smi -n linkerd-smi --create-namespace linkerd-smi/linkerd-smi

helm repo add linkerd https://helm.linkerd.io/stable
helm repo up
helm install --kube-context=minikube linkerd-failover -n linkerd-failover --create-namespace linkerd/linkerd-failover
```

Mesh the required services and deployments.

```
kubectl -n reconstruct --context=tnok8s label svc jaeger-collector mirror.linkerd.io/exported=true

kubectl -n reconstruct get deploy viewer -o yaml | linkerd inject - | kubectl -n reconstruct apply -f -

kubectl -n reconstruct get deploy otel-collector -o yaml | linkerd inject - | kubectl -n reconstruct apply -f -
```

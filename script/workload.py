import os
import requests

# URL to which you want to send the POST request
url = 'http://localhost/upload'
folder_path = './skull3'
# Files to be sent in the POST request
files = [
        ('file', (filename, open(os.path.join(folder_path, filename), 'rb')))
        for i, filename in enumerate(os.listdir(folder_path))
    ]

# Additional data (if needed)
data = {'chunks': 'value'}

try:
    response = requests.post(url, files=files, data=data)
    
    if response.status_code == 200:
        print('Files uploaded successfully!')
        print('Response:', response.text)
    else:
        print('Failed to upload files. Status Code:', response.status_code)

except requests.exceptions.RequestException as e:
    print('An error occurred:', e)

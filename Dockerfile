FROM openmvg
WORKDIR /usr/src/app
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update && apt install -y curl && apt install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt install -y python3.9 python3.9-distutils python3.9-venv python3.9-dev && python3.9 --version && \
    python3.9 -m ensurepip && pip3.9 --version

RUN pip3.9 install opentelemetry-api opentelemetry-sdk opentelemetry-exporter-otlp-proto-grpc

RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
RUN curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash

RUN apt-get upgrade -y && apt-get update && apt-get dist-upgrade

RUN apt install -y libmariadb3 libmariadb-dev awscli
RUN pip3.9 install -U flask[async] mariadb boto3 Werkzeug
RUN pip3.9 install requests-futures nest-asyncio
COPY . /usr/src/app
#RUN apt-get install -y wget && apt-get install -y unzip
#RUN wget https://github.com/kovacsv/Online3DViewer/releases/latest/download/o3dv.zip
#RUN mv o3dv.zip templates && cd templates && unzip o3dv.zip
ENV AWS_ACCESS_KEY_ID=AKIA6PROQZJZWE462XQL
ENV AWS_SECRET_ACCESS_KEY=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
ENV AWS_DEFAULT_REGION=eu-west-1
ENTRYPOINT [ "python3.9" ]

CMD ["-u", "app.py" ]


import os
from socket import MsgFlag
import subprocess
import zipfile
import uuid
import requests
import shutil 
import asyncio 
import nest_asyncio
import time
import math
import typing
import threading
import aiohttp
import json
nest_asyncio.apply()

from flask import Flask, render_template, request, redirect, send_file, jsonify
from werkzeug.utils import secure_filename
from s3demo import list_files, upload_file, download_file, download_named_files, download_folder, check_s3_connection, upload_file_async
from opentelemetry import trace, propagators
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider, Span
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
from opentelemetry.context import Context
#from db import executequery,checkrequestid, insertrequestid, updaterequestid, getconnection, updatesequenceprogress, checkrequestprogress
from os import path, makedirs, listdir
from os.path import isfile, join
from concurrent.futures import as_completed
from requests_futures.sessions import FuturesSession
from opentelemetry.trace import set_span_in_context
from prometheus_client import Counter, generate_latest
from flask_cors import CORS
from opentelemetry.instrumentation.aiohttp_client import AioHttpClientInstrumentor, create_trace_config



# Service name is required for most backends
resource = Resource(attributes={
    SERVICE_NAME: "AppViewer"
})

class SpanProcessor(BatchSpanProcessor):
    def on_start(self, span: Span, parent_context: typing.Optional[Context] = None) -> None:
        span.set_attribute("thread.id", threading.get_native_id())
        #span.add_event("AppViewer")

app = Flask(__name__)
CORS(app)
provider = TracerProvider(resource=resource)
processor = SpanProcessor(OTLPSpanExporter(insecure=True))
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)

def request_hook(span: Span, params: aiohttp.TraceRequestStartParams):
   if span and span.is_recording():
       print(params.headers)
       span.add_event("message",{"message": params.headers.get("message")})
       span.end()

def response_hook(span: Span, params: typing.Union[
             aiohttp.TraceRequestEndParams,
             aiohttp.TraceRequestExceptionParams,
         ]):
     with tracer.start_as_current_span("PostResponse") as responsespan:
         responsespan.add_event("message",{"message": f'{params.headers.get("message")}Resp'})


#AioHttpClientInstrumentor().instrument(request_hook=request_hook, response_hook=response_hook, tracer_provider=trace.get_tracer_provider())

total_count = Counter('reconstruction_count', 'total 3dreconstruction count', ['Longitude', 'latitude'])
# index
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/metrics')
def metrics():
    time.sleep(10)
    return generate_latest()

@app.route('/view/<folder>', methods=['GET'])
def view(folder):
    print('In View')
    print(folder)
    return render_template('index.html', contents=folder)

# uploading file to s3
@app.route('/upload', methods=['POST'])
async def upload():
    session = FuturesSession()
    carrier = {}
    with tracer.start_as_current_span("Client") as recontructionspan:
        recontructionspan.end
        TraceContextTextMapPropagator().inject(carrier)
        header = {"traceparent": carrier["traceparent"]}
        files = request.files.getlist("file")
        print("files")
        print(files)
        folder = uuid.uuid4().hex
        cwd = f'download/{folder}'
        os.makedirs(cwd)
        with tracer.start_as_current_span("SaveFiles") as savespan:
            for file in files:
                file.save(os.path.join(cwd, secure_filename(file.filename)))
        s3connection = check_s3_connection()
        futures=[]
        onlyfiles = next(os.walk(cwd))[2]
        onlyfiles.sort()
        tasks=[]
        failedimages=[]
        if request.form.get('chunks') != None and s3connection :
            print(onlyfiles)
            counter = 0
            sequence = 1
            filenames = []
            first = True
            for file in onlyfiles:
                counter += 1
                tasks.append(asyncio.create_task(upload_file_async(f'download/{folder}/{file}', tracer)))
                filenames.append(file)
                if counter == 4 or file == onlyfiles[-1] :
                    #with tracer.start_as_current_span("UploadBatch") as uploadspan:
                    await asyncio.gather(*tasks)
                    counter = 0
                    api_url = f'{os.environ["OPENMVG_URL"]}/openmvgchunks'
                    print(filenames)
                    scanstatus = 'inprogress'
                    if file == onlyfiles[-1]:
                        scanstatus = 'done'
                    data = {'requestID':folder, 'scanstatus':scanstatus, 'filenames': ','.join(filenames), 'sequence':sequence, 'totalchunks': math.ceil(len(onlyfiles)/4)}
                    sequence += 1
                    #futures.append(session.post(url = api_url, json = data, headers=header ))
                    futures.append(asyncio.create_task(callAioHttp( api_url, data, header, sequence-1)))
                    #task = asyncio.create_task(callAioHttp( api_url, data, header, sequence-1))
                    #await task
                    #time.sleep(50)
                    filenames = []
            #for future in futures:
            #    response = future.result()
            response = await asyncio.gather(*futures)
            responseid = response[0]
            print("response", response[0])
        else:
            if s3connection :
                #archived = shutil.make_archive(f'download/{folder}', 'zip', f'download/{folder}')
                for file in onlyfiles:
                    await upload_file(f'download/{folder}/{file}', tracer)

            api_url = f'{os.environ["OPENMVG_URL"]}/openmvg'
            data = {'filename':folder}
            futures.append(asyncio.create_task(callAioHttp( api_url, data, header, 0)))
            response = await asyncio.gather(*futures)
            resp = json.loads(response[0])
            responseid  =   resp["folder"]
            failedimages = resp["failedimages"]
            print("response.text : "+responseid)
    return render_template('index.html', contents=responseid, remotelocation=s3connection, failedimages=failedimages)

async def callAioHttp(url, jsondata, header, seq):
    print('callAioHttp ',jsondata)
    timeout = aiohttp.ClientTimeout(total=600)
    async with aiohttp.ClientSession(timeout=timeout, trace_configs=[create_trace_config(request_hook=request_hook, response_hook=response_hook)]) as session:
        #await asyncio.sleep(seq)
        if "sequence" in jsondata:
            header["message"]=f'OpenMVGCall{jsondata["sequence"]}'
        else :
            header["message"]='OpenMVGCall'
        async with session.post(url, json=jsondata, headers=header) as response:
            resp = await response.text()
            print('callAioHttp success ',jsondata)
            return resp

# downloading file from s3
@app.route('/download/<filename>', methods=['GET'])
@app.route('/<filename>', methods=['GET'])
def download(filename):
    print('Downloading file = {}'.format(filename))
    #output = download_file(filename)

    return send_file('download/'+filename, as_attachment=True)

@app.route('/image/<folder>/<filename>', methods=['GET'])
def getimage(folder,filename):
    print('Downloading file = {}'.format(filename))
    return send_file('download/'+folder+'/'+filename, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')
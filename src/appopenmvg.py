import os
import subprocess
import zipfile
import uuid
import requests
import shutil 
import asyncio 
import nest_asyncio
import time
import math
import typing
import threading
import json
nest_asyncio.apply()

from flask import Flask, render_template, request, redirect, send_file, jsonify
from werkzeug.utils import secure_filename
from s3demo import list_files, upload_file, download_file, download_named_files, download_folder
from opentelemetry import trace, propagators
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider, Span
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
from opentelemetry.context import Context
from db import executequery,checkrequestid, insertrequestid, updaterequestid, getconnection, updatesequenceprogress, checkrequestprogress
from os import path, makedirs, listdir
from os.path import isfile, join
from concurrent.futures import as_completed
from requests_futures.sessions import FuturesSession

class SpanProcessor(BatchSpanProcessor):
    def on_start(self, span: Span, parent_context: typing.Optional[Context] = None) -> None:
        span.set_attribute("thread.id", threading.get_native_id())
        span.add_event("OpenMVG")

# Service name is required for most backends
resource = Resource(attributes={
    SERVICE_NAME: "OpenMVG"
})

app = Flask(__name__)

provider = TracerProvider(resource=resource)
processor = SpanProcessor(OTLPSpanExporter(insecure=True))
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)

# pre-requisites -
# 1. in the project directory remember to create the "upload" and "download" folders
# 2. aws iam cli user is already created and have s3 full access policy attached
# 3. aws s3 bucket is already created


# creating the endpoints

# index
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/view/<folder>', methods=['GET'])
def view(folder):
    print('In View')
    print(folder)
    return render_template('index.html', contents=folder)


# downloading file from s3
@app.route('/download/<filename>', methods=['GET'])
@app.route('/<filename>', methods=['GET'])
def download(filename):
    print('Downloading file = {}'.format(filename))
    #output = download_file(filename)

    return send_file('download/'+filename, as_attachment=True)

@app.route('/openmvgchunks', methods=['POST'])
async def openmvgchunks():
    traceparent = request.headers.get_all("traceparent")
    carrier = {"traceparent": traceparent[0]}    
    ctx = TraceContextTextMapPropagator().extract(carrier)
    with tracer.start_as_current_span("OpenMVG", context=ctx) as recontructionspan:
        content = request.json
        requestID = content['requestID']
        filenames   =   content['filenames']
        sequence = content['sequence']
        print(content) 
        conn = getconnection()
        recontructionspan.add_event("message",{"message":request.headers.get("message")})
        with tracer.start_as_current_span("insertrequestid") as insertspan:
            insertrequestid(conn, requestID, sequence, content['totalchunks'])
        os.makedirs(f"download/{requestID}/{sequence}", exist_ok = True)
        #subprocess.call(['mkdir', f"download/{requestID}/{sequence}"])
        tasks=[]
        for file in filenames.split(","):
            tasks.append(asyncio.create_task(download_named_files(requestID, file , sequence, tracer)))
        await asyncio.gather(*tasks)
        #if checkrequestid(conn, requestID) :
        with tracer.start_as_current_span("ComputeFatures") as localizationspan:                     
            subprocess.run(["openMVG_main_SfMInit_ImageListing", "-i", ".", "-o", ".", "-d", "/opt/openMVG/src/openMVG/exif/sensor_width_database/sensor_width_camera_database.txt", "-f", "1800"], cwd=f"download/{requestID}/{sequence}")
            #subprocess.run(['mv', '-f' , f"reconstruction_sequential/sfm_data_expanded.json", f"reconstruction_sequential/sfm_data.json"], cwd=f"download/{requestID}")
            subprocess.run( ["openMVG_main_ComputeFeatures",  "-i", "./sfm_data.json", "-o", "../matches","-m", "SIFT"] , cwd=f"download/{requestID}/{sequence}")
            updatesequenceprogress(conn, requestID, sequence, "completed")
        if not checkrequestprogress(conn, requestID) :
            with tracer.start_as_current_span("Sfm_Pipeline") as pipelinespan:
                    print("Running sequence ", sequence)
                    subprocess.call("rsync -a --include={*.[jJ][pP][gG],*.[Pp][Nn][Gg],*.[jJ][pP][eE][gG]} --exclude=* ./*/ ./", cwd=f"download/{requestID}", shell=True, executable='/bin/bash')
                    subprocess.run(["python3", "/usr/src/app/SfM_SequentialPipeline.py", "." , ".", "INCREMENTALV2",'false'], cwd=f"download/{requestID}")
            with tracer.start_as_current_span("openMVG2openMVS") as mvg2mvsspan:
                subprocess.run(["openMVG_main_openMVG2openMVS", "-i", "reconstruction_sequential/sfm_data.json", "-d", ".", "-o", "scene.mvs"], cwd=f"download/{requestID}")
                await upload_file(f"download/{requestID}/scene.mvs",tracer)
            with tracer.start_as_current_span("CallOpenMVS") as mvsspan:
                mvsspan.add_event("message",{"request":"OpenMVSCall"})
                #openmvs_endpoint = "http://openmvs:5000/openmvs"
                openmvs_endpoint = f'{os.environ["OPENMVS_URL"]}/openmvs'
                data = {'folder':requestID}
                carrier = {}
                TraceContextTextMapPropagator().inject(carrier)
                header = {"traceparent": carrier["traceparent"]}
                r = requests.post(url = openmvs_endpoint, json = data, headers=header )
                mvsspan.add_event("message",{"reply":"OpenMVSReply"})
                updaterequestid(conn, requestID, "Done")
        #else:
            # with tracer.start_as_current_span("SfM_SequentialPipeline") as pipelinespan: 
            #     print("doesn't exit")
            #     #with tracer.start_as_current_span("DatabaseInsert") as databasespan: 
            #         #checkrequestid(conn, requestID)
            #     with tracer.start_as_current_span("SequentialPipeline") as pipelinespan:
            #         print("Running sequence ", sequence)
            #         subprocess.run(["python3", "/usr/src/app/SfM_SequentialPipeline.py", "." , "..", "INCREMENTAL"], cwd=f"download/{requestID}/{sequence}")
            #     updatesequenceprogress(conn, requestID, sequence, "completed")
        with tracer.start_as_current_span("OpenMVGResponse") as respspan:
            respspan.add_event("message",{"message":f'{request.headers.get("message")}Resp'})
            conn.close()
            return requestID

@app.route('/openmvg', methods=['POST'])
async def openmvg():
    traceparent = request.headers.get_all("traceparent")
    carrier = {"traceparent": traceparent[0]}    
    ctx = TraceContextTextMapPropagator().extract(carrier)
    with tracer.start_as_current_span("OpenMVG", context=ctx) as recontructionspan: 
        content = request.json
        folder = content['filename']
        #folder = filename
        recontructionspan.add_event("message",{"message":request.headers.get("message")})
        if os.environ["INSTALL_MODE"] != "CLIENT" :
            with tracer.start_as_current_span("DownloadFromS3") as downloadspan: 
                #folder = uuid.uuid4().hex
                subprocess.run(["mkdir","-p", f"download/{folder}"])
                #output = download_file(f'{filename}.zip', folder)
                download_folder(folder)
                #print('Unziping file ')
                #with zipfile.ZipFile(output, 'r') as zip_ref:
                #    zip_ref.extractall(f'download/{folder}')

        with tracer.start_as_current_span("SfM_SequentialPipeline") as pipelinespan: 
            print('Running openmvg pipeline')
            subprocess.run(["python3", "/usr/src/app/SfM_SequentialPipeline.py", "." , ".", "INCREMENTALV2", 'true'], cwd=f"download/{folder}")
        with tracer.start_as_current_span("openMVG2openMVS") as conversionspan: 
            print('Running openmvg to openmvs conversion')
            subprocess.run(["openMVG_main_openMVG2openMVS", "-i", "reconstruction_sequential/sfm_data.bin", "-d", ".", "-o", "scene.mvs"], cwd=f"download/{folder}")
        
        if os.environ["INSTALL_MODE"] != "CLIENT" :
            with tracer.start_as_current_span("UploadtoS3") as uploadspan:
                print('Uploading MVS')
                await upload_file(f"download/{folder}/scene.mvs", tracer)
    
        carrier = {}
        TraceContextTextMapPropagator().inject(carrier)
        header = {"traceparent": carrier["traceparent"]}
        with tracer.start_as_current_span("CallOpenMVS") as mvsspan:
            mvsspan.add_event("message",{"request":"OpenMVSCall"})
            #openmvs_endpoint = "http://openmvs:5000/openmvs"
            openmvs_endpoint = f'{os.environ["OPENMVS_URL"]}/openmvs'
            data = {'folder':folder}
            r = requests.post(url = openmvs_endpoint, json = data,  headers=header)
            with tracer.start_as_current_span("OpenMVSResp") as mvsresp:
                mvsresp.add_event("message",{"reply":"OpenMVSReply"})
        with tracer.start_as_current_span("OpenMVGResponse") as respspan:
            respspan.add_event("message",{"message":"OpenMVGCallResp"})
            missing = failedimages(folder)
    return jsonify(folder=folder, failedimages = missing)

def failedimages(folder):
    with open(f"download/{folder}/reconstruction_sequential/sfm_data.json", 'r') as f:
        data = json.load(f)
    views = {}
    for item in data['views']:
        views[item['key']] = item['value']['ptr_wrapper']['data']['filename']
    print(views)
    extrinsics = set()
    for item in data['extrinsics']:
        extrinsics.add(item['key'])
    print(extrinsics)
    print(set(views.keys()) - extrinsics)
    missing = []
    for item in set(views.keys()) - extrinsics:
        print(views[item])
        missing.append(views[item])
    
    return missing

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')

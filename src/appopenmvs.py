import os
import subprocess
import zipfile
import typing
import threading
from flask import Flask, render_template, request, redirect, send_file, jsonify
from werkzeug.utils import secure_filename

from s3demo import list_files, upload_file, download_file, download_file_Fromfolder

from flask import Flask, render_template, request, redirect, send_file, jsonify
from werkzeug.utils import secure_filename
from s3demo import list_files, upload_file, download_file, download_folder
from opentelemetry import trace, propagators
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider, Span
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
from opentelemetry.context import Context
# Service name is required for most backends
resource = Resource(attributes={
    SERVICE_NAME: "OpenMVS"
})


class SpanProcessor(BatchSpanProcessor):
    def on_start(self, span: Span, parent_context: typing.Optional[Context] = None) -> None:
        span.set_attribute("thread.id", threading.get_native_id())
        span.add_event("OpenMVS")

app = Flask(__name__)

provider = TracerProvider(resource=resource)
processor = SpanProcessor(OTLPSpanExporter(insecure=True))
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)

# pre-requisites -
# 1. in the project directory remember to create the "upload" and "download" folders
# 2. aws iam cli user is already created and have s3 full access policy attached
# 3. aws s3 bucket is already created


# creating the endpoints

# index
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


# uploading file to s3
@app.route('/upload', methods=['POST'])
def upload():
    f = request.files['file']
    if f.filename:
        print('Uploading file = {}'.format(f.filename))
        # secure_filename function will replace any whitespace provided filename with an underscore
        # saving the file in the local folder
        f.save(os.path.join('upload', secure_filename(f.filename)))
        upload_file(f'upload/{secure_filename(f.filename)}')
    else:
        print('Skipping file upload op')

    return redirect('/')


# downloading file from s3
@app.route('/download/<filename>', methods=['GET'])
def download(filename):
    print('Downloading file = {}'.format(filename))
    output = download_file(filename)

    return send_file(output, as_attachment=True)


@app.route('/openmvs/', methods=['POST'])
async def openmvs():
    traceparent = request.headers.get_all("traceparent")
    carrier = {"traceparent": traceparent[0]}    
    ctx = TraceContextTextMapPropagator().extract(carrier)
    content = request.json
    folder = content.get('folder')
    dataset =   content.get('dataset')
    subprocess.run(["mkdir", f"download/{folder}"])
    with tracer.start_as_current_span("OpenMVS", context=ctx) as mvsspan:
        mvsspan.add_event("message",{"request":"OpenMVSCall"})
        if os.environ["INSTALL_MODE"] != "CLIENT" :
            with tracer.start_as_current_span("S3_Download") as downloadspan: 
                #print('Downloading file = {}'.format(dataset))
                output = download_file_Fromfolder('scene.mvs', folder)
                if dataset:
                    output = download_file(dataset, folder)
                    print('Unziping file ')
                    with zipfile.ZipFile(output, 'r') as zip_ref:
                        zip_ref.extractall(f'download/{folder}')
                else :
                    download_folder(folder)
        with tracer.start_as_current_span("DensifyPointCloud") as densifyspan: 
            print('Running openmvs DensifyPointCloud')
            #subprocess.run(["DensifyPointCloud", "--resolution-level=1","scene.mvs"], cwd=f'download/{folder}')
            subprocess.run(["DensifyPointCloud", "scene.mvs"], cwd=f'download/{folder}')
        with tracer.start_as_current_span("ReconstructMesh") as densifyspan: 
            print('Running openmvs ReconstructMesh')
            subprocess.run(["ReconstructMesh", "scene_dense.mvs"], cwd=f'download/{folder}')
        with tracer.start_as_current_span("RefineMesh ") as densifyspan: 
            print('Running openmvs RefineMesh ')
            subprocess.run(["RefineMesh", "scene_dense_mesh.mvs"], cwd=f'download/{folder}')
        with tracer.start_as_current_span("TextureMesh") as densifyspan: 
            print('Running openmvs TextureMesh')
            subprocess.run(["TextureMesh", "--export-type=glb" ,"scene_dense_mesh_refine.mvs"], cwd=f'download/{folder}')
        with tracer.start_as_current_span("OpenMVSResponse") as mvsresp: 
            mvsresp.add_event("message",{"reply":"OpenMVSReply"})
            if os.environ["INSTALL_MODE"] != "CLIENT" :
                print('Uploading dense MVS')
                await upload_file(f"download/{folder}/scene_dense_mesh_refine_texture.glb", tracer)
                await upload_file(f"download/{folder}/scene_dense_mesh_refine_texture.png", tracer)
    
    return {}

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')

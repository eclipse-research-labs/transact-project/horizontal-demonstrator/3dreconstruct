import json
import mariadb
import time
import os

config = {
    'host': f'{os.environ["DB_HOST"]}',
    'port': int(f'{os.environ["DB_PORT"]}'),
    'user': '3dclient',
    'password': '3dclient',
    'database': 'reconstruct',
    'autocommit': False
}
#conn = mariadb.connect(**config)
#cur = conn.cursor()

def getconnection():
    print("getconnection")
    conn = mariadb.connect(**config)
    return conn

def executequery():
   # execute a SQL statement
   cur.execute("select * from reconstruction")
   rv = cur.fetchall()
   for result in rv:
        print(result)

def checkrequestid(conn , requestid):
    print("checkrequestid")
    cur = conn.cursor()
    cur.execute("select * from reconstruction where requestid= ? for update", (requestid,))
    exist = cur.fetchone()
    print("locked")
    cur.execute("select * from reconstruction_progress where requestid= ? and progress = ?", (requestid,"completed"))
    exist = cur.fetchone()
    if exist is None:
        return False
    return True

def insertrequestid(conn , requestid, sequence, totalchunks):
    print("insert into reconstruction")
    try: 
        cur = conn.cursor()
        cur.execute("insert IGNORE into reconstruction values(?,?, ?)", (requestid,"inprogress", totalchunks))
        cur.execute("insert into reconstruction_progress values(?,?,?)",(requestid, sequence,"inprogress" ))
    except mariadb.Error as e: 
        print(f"Error: {e}")
        conn.rollback()
        raise  
    conn.commit()
    return True

def updaterequestid(conn , requestid, status):
    print("update reconstruction")
    try: 
        cur = conn.cursor()
        cur.execute("update reconstruction set status=? where requestid= ?", (status , requestid))
    except mariadb.Error as e: 
        print(f"Error: {e}")
        conn.rollback()
        raise  
    conn.commit()
    return True

def updatesequenceprogress(conn , requestid, sequence, status):
    print("update reconstruction Progress")
    try: 
        cur = conn.cursor()
        cur.execute("update reconstruction_progress set progress=? where requestid= ? and sequence = ?", (status , requestid, sequence))
    except mariadb.Error as e: 
        print(f"Error: {e}")
        conn.rollback()
        raise  
    conn.commit()
    return True


def checkrequestprogress(conn , requestid):
    print("checkrequestprogress")
    try: 
        cur = conn.cursor()
        cur.execute("select count(*) from reconstruction_progress where progress='completed' and requestid = ? having count(*) = \
                        (select totalchunks from reconstruction where requestid = ?)", (requestid, requestid))
        exist = cur.fetchone()
        if exist is None:
            return True
        return False
    except mariadb.Error as e: 
        print(f"Error: {e}")
        conn.rollback()
        raise  
    conn.commit()
    return True


def waitforequestidstatus(requestid):
    print("wait for reconstruction status")
    try: 
        while 1:
            q   =   cur.execute("select status from reconstruction where requestid= ?", (requestid)).fetchone()
            if q[0] == "done":
                break
            time.sleep(1)  
    except mariadb.Error as e: 
        print(f"Error: {e}")
        conn.rollback()
        raise  
    return True
const loading = document.getElementById("loading");
const button = document.getElementById("button");
const model3d = document.getElementById("model3d");
const rejectedimages = document.getElementById("rejectedimages");
const viewer = document.getElementById("viewer");
const outdated = document.getElementById("outdated");
button.addEventListener("click", (event) => {
    loading.classList.add("loading")
    model3d.style.display = "none"
    rejectedimages.style.display = "none"
    outdated.style.display = "inline"
});


const modelViewerTexture = document.querySelector('#model3d');            
modelViewerTexture.addEventListener("load", () => {

const material = modelViewerTexture.model.materials[0];
console.log("on load 0");
const texture = modelViewerTexture.createTexture("https://tno3dreconstructiondemo.s3.eu-west-1.amazonaws.com/{{ contents }}/scene_dense_mesh_refine_texture.png");
    // Set the texture name
material["normalTexture"].setTexture(texture);
console.log("on load 1");
const createAndApplyTexture = async (channel, event) => {
        console.log("on load 2");
    // Creates a new texture.
    const texture = await modelViewerTexture.createTexture("download/scene_dense_mesh_refine_texture.png");
    // Set the texture name
    //texture.name = event.target.options[event.target.selectedIndex].text.replace(/ /g, "_").toLowerCase();
    // Applies the new texture to the specified channel.
    material[channel].setTexture(texture);
    console.log("on load 3");
    
}
});
import asyncio 
import time
import aioboto3

from botocore.exceptions import ClientError
from os import path, makedirs

from config import create_s3_client

# bucket name
# note - we are considering that the bucket already exists in aws environment.
# we can have a check_bucket function() to check whether the bucket exists or not
BUCKET = 'tno3dreconstructiondemo'

# s3 client instance to perform the s3 related operations
s3_client = create_s3_client()
session = aioboto3.Session()
# check S3 Connection
def check_s3_connection():
    try:
        response = s3_client.head_bucket(Bucket=BUCKET)
        print("S3 Coonection Success")
        return True
    except:
        print("S3 Connection Failed")
    return False

# get the list of files from s3 bucket
def list_files():
    contents = []
    try:
        response = s3_client.list_objects_v2(Bucket=BUCKET)
        if 'Contents' in response:
            for item in response['Contents']:
                # print(item)
                contents.append(item)
        else:
            print('Bucket is empty')
    except ClientError as e:
        print(e)

    return contents


# upload a file to s3 bucket
async def upload_file(file_location, tracer):
    with tracer.start_as_current_span("UploadFile") as uploadspan:
        try:
            print('File uploaded started', file_location)
            path, name = file_location.split('/',1)
            response = s3_client.upload_file(file_location, BUCKET, name)
            print('File uploaded successfully', file_location)
        except ClientError as e:
            print(e)

# upload a file to s3 bucket
async def upload_file_async(file_location , tracer):
    async with session.client("s3") as s3:
        try:
            path, name = file_location.split('/',1)
            with tracer.start_as_current_span("UploadFile") as uploadspan:
                await s3.upload_file(file_location, BUCKET, name)
        except Exception as e:
            print(e)

# download a file from s3 bucket
def download_file(file_name, folder):
    print('Downloading file = {}'.format(file_name))
    destination = f"download/{folder}/{file_name}"
    try:
        s3_client.download_file(BUCKET, file_name, destination)
        print('File downloaded successfully')
    except ClientError as e:
        print(e)

    return destination

def download_folder(folder):
    try:
        response = s3_client.list_objects_v2(Bucket=BUCKET,Prefix=folder)
        if 'Contents' in response:
            for item in response['Contents']:
                print(item)
                destination = f"download/{item['Key']}"
                if not path.exists(path.dirname(destination)):
                    makedirs(path.dirname(destination))
                s3_client.download_file(BUCKET, item['Key'], f"download/{item['Key']}")
        else:
            print('Bucket is empty')
        print('File downloaded successfully')
    except ClientError as e:
        print(e)

    return destination

# download a file from s3 bucket
def download_file_Fromfolder(file_name, folder):
    destination = f"download/{folder}/{file_name}"
    path, name = destination.split('/',1)
    
    try:
        s3_client.download_file(BUCKET, name, destination)
        print('File downloaded successfully')
    except ClientError as e:
        print(e)

    return destination

async def download_named_files(requestid, filename, sequence, tracer):
    with tracer.start_as_current_span("DownloadFile") as uploadspan:
        #for file in filenames:
        destination = f"download/{requestid}/{sequence}/{filename}"
        try:
            s3_client.download_file(BUCKET, f"{requestid}/{filename}", destination)
            print('File downloaded successfully')
        except ClientError as e:
            print(e)